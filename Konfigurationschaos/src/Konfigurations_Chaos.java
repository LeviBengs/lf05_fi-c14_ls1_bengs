
public class Konfigurations_Chaos {
	
	public static void main(String[] args) {
		
		String bezeichnung = "Q2021_FAB_A";
		String typ = "Automat AVR";
		String name = typ + " " + bezeichnung;
		char sprachModul = 'd';
		final int PRUEFNR = 4;
		int muenzenCent = 1280;
		int muenzenEuro = 130;
		int summe = muenzenCent + muenzenEuro * 100;
		double maximum = 100.00;
		double patrone = 46.24;
		double fuellstand = maximum - patrone;
		byte cent;
		cent = (byte) (summe % 100);
		int euro = (summe / 100); 
		boolean statusCheck;
		statusCheck = (euro <= 150) 
				&& (euro >= 50)
				&& (cent != 0)
				&& (sprachModul == 'd')
				&& (fuellstand >= 50.00) 
				&&  (!(PRUEFNR == 5 || PRUEFNR == 6)); 




		


		
		System.out.println("Name: " + name);
		System.out.println("Sprache: " + sprachModul);
		System.out.println("Pr�fnummer : " + PRUEFNR);
		System.out.println("F�llstand Patrone: " + fuellstand + " %");
		System.out.println("Summe Euro: " + euro +  " Euro");
		System.out.println("Summe Rest: " + cent +  " Cent");		
		System.out.println("Status: " + statusCheck);

	}
}
