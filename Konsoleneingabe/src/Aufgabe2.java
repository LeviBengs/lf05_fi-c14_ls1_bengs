import java.util.Scanner;
public class Aufgabe2 {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner (System.in);
		
		System.out.print("Hallo Lieber Nutzer, wie ist ihr Name?: ");
		String Name = myScanner.next();
		
		System.out.print("Wie alt sind Sie?: ");
		int Alter = myScanner.nextInt();
		 
		System.out.print("Hallo " + Name + " Sie sind " + Alter + " Jahre alt");
		myScanner.close();
	}

}
