import java.util.Scanner;
public class Rechner_Me {

	public static void main(String[] args) {
		boolean run = true; 
		while(run ) {
		Scanner MyScanner = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie die Rechnung ein:");
		
		Float x = MyScanner.nextFloat();
		Float y = MyScanner.nextFloat();
		char z = MyScanner.next().charAt(0);
		
		System.out.print("Das Ergebnis lautet: ");
		if(z == '+') System.out.print(x+y);
		if(z == '-') System.out.print(x-y);
		if(z == '/') System.out.print(x/y);
		if(z == '*') System.out.print(x*y);
		
		String answer; 
		System.out.print("\nWollen Sie eine neue Rechnung Eingeben?\n");
		answer = MyScanner.next();
		if(answer.equals("Nein"))System.out.print("Auf Wiedersehen");
		
		run = answer.equals("Ja");
		
		
		}
		
	}
	

}
