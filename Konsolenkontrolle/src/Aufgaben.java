
public class Aufgaben {
	public static void main(String[] args) {
		
		// AB 1 Aufgabe 1
		System.out.println("Arbeitsblatt 1 Aufgabe 1 \n");
		
		System.out.println("Hallo ich hei�e \"Levi\"\n" + "Ich bin 18 Jahre Jung.\n");
		// AB 1 Aufgabe 2
		System.out.println("Arbeitsblatt 1 Aufgabe 2 \n");
		
		System.out.println("       *\n"
				+ "      ***\n"
				+ "     *****\n"
				+ "    *******\n"
				+ "   *********\n"
				+ "  ***********\n"
				+ " *************\n"
				+ "      ***\n"
				+ "      ***\n");
		
		// Ab 1 Aufgabe 3
		System.out.println("Arbeitsblatt 1 Aufgabe 3 \n");
		
		double a = 22.4234234;
		double b = 111.2222;
		double c = 4.0;
		double d = 1000000.551;
		double e = 97.34;
		System.out.printf( "%.2f\n" ,a);
		System.out.printf( "%.2f\n" ,b);
		System.out.printf( "%.2f\n" ,c);
		System.out.printf( "%.2f\n" ,d);
		System.out.printf( "%.2f\n" ,e);
		
		// AB 2 Aufgabe 1
		System.out.println("Arbeitsblatt 2 Aufgabe 1\n");
		
		String s = "*";
		
		System.out.printf("%20s \n" , s + s);
		System.out.printf("%17s" , s); System.out.printf("%5s \n" , s);
		System.out.printf("%17s" , s); System.out.printf("%5s \n" , s);	
		System.out.printf("%20s \n" , s + s);
		
		// AB 2 Aufgabe 2
		System.out.println("Areitsblatt 2 Aufgabe 2\n");
		
		System.out.printf( "%-5s", "0!" ); System.out.printf("="); System.out.printf("%-19s" , ""); System.out.printf("=");System.out.printf("%4s\n", "1");
		System.out.printf( "%-5s", "1!" ); System.out.printf("="); System.out.printf("%-19s" , ""); System.out.printf("=");System.out.printf("%4s\n" , "1");
		System.out.printf( "%-5s", "2!" ); System.out.printf("="); System.out.printf("%-19s" , " 1 * 2"); System.out.printf("=");System.out.printf("%4s\n" , "2");
		System.out.printf( "%-5s", "3!" ); System.out.printf("="); System.out.printf("%-19s" , " 1 * 2 * 3"); System.out.printf("=");System.out.printf("%4s\n" , "6");
		System.out.printf( "%-5s", "4!" ); System.out.printf("="); System.out.printf("%-19s" , " 1 * 2 * 3 * 4"); System.out.printf("=");System.out.printf("%4s\n" , "24");
		System.out.printf( "%-5s", "5!" ); System.out.printf("="); System.out.printf("%-19s" , " 1 * 2 * 3 * 4 * 5"); System.out.printf("=");System.out.printf("%4s\n" , "120");
	
		// AB 2 Aufgabe 3
		System.out.println("\nArbeitsblatt 2 Aufgabe 3\n");
		
		System.out.printf("%-12s | %10s \n" , "Fahrenheit" , "Celsius");
		System.out.printf("%23s \n" , "-------------------------");
		System.out.printf("%-12s | %10s \n" , "-20", "-28.89");
		System.out.printf("%-12s | %10s \n" , "-10", "-23.33");
		System.out.printf("%-12s | %10s \n" , "+ 0", "-17.78");
		System.out.printf("%-12s | %10s \n" , "+20", "-6.67");
		System.out.printf("%-12s | %10s \n" , "+30", "-1.11");
		
		
	} 
}
