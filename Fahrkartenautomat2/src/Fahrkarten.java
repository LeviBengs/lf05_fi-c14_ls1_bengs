import java.util.Scanner;

public class Fahrkarten {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		
		// 		double Einzelfahrausweis_Berlin_AB;
		// 		double Einzelfahrausweis_Berlin_BC;
		//		double Einzelfahrausweis_Berlin_ABC;
		//		double Kurzstrecke;
		// 		double Tageskarte_Berlin_AB;
		//		double Tageskarte_Berlin_BC;
		//		double Tageskarte_Berlin_ABC;
		//		double Kleingruppenkarte_Berlin_AB;
		//		double Kleingruppenkarte_Berlin_BC;
		//		double Kleingruppenkarte_Berlin_ABC;
		double eingeworfeneM�nze;
	    double r�ckgabebetrag;
		double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag = 0;
		double Nichts = 0;
		double Einzelfahrausweis_Berlin_AB = 2.90;
		double Einzelfahrausweis_Berlin_BC = 3.30;
		double Einzelfahrausweis_Berlin_ABC = 3.60;
		double Kurzstrecke = 1.90;
		double Tageskarte_Berlin_AB = 8.60;
		double Tageskarte_Berlin_BC = 9.00;
		double Tageskarte_Berlin_ABC = 9.60;
		double Kleingruppenkarte_Berlin_AB = 23.50;
		double Kleingruppenkarte_Berlin_BC = 24.30;
		double Kleingruppenkarte_Berlin_ABC = 24.90;

    	double[] karten = {Nichts , Einzelfahrausweis_Berlin_AB, Einzelfahrausweis_Berlin_BC, Einzelfahrausweis_Berlin_ABC, Kurzstrecke, Tageskarte_Berlin_AB, Tageskarte_Berlin_BC, Tageskarte_Berlin_ABC, Kleingruppenkarte_Berlin_AB, Kleingruppenkarte_Berlin_BC, Kleingruppenkarte_Berlin_ABC};
			System.out.print("Welche Karte wolle Sie kaufen?\r\n"
					+ "1. Einzelfahrausweis Berlin AB\r\n"
					+ "2. Einzelfahrausweis Berlin BC\r\n"
					+ "3. Einzelfahrausweis Berlin ABC\r\n"
					+ "4. Kurzstrecke\r\n"
					+ "5. Tageskarte_Berlin AB\r\n"
					+ "6. Tageskarte_Berlin BC\r\n"
					+ "7. Tageskarte_Berlin ABC\r\n"
					+ "8. Kleingruppenkarte Berlin AB\r\n"
					+ "9. Kleingruppenkarte Berlin BC\r\n"
					+ "10. Kleingruppenkarte Berlin ABC\r\n");
			
				System.out.print("Welche Fahrkarte wollen Sie Kaufen?");
				zuZahlenderBetrag = karten[tastatur.nextInt()];
			 	System.out.print("Zuzahlender Betrag: " + zuZahlenderBetrag);
			
				while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
		       {
		    	   System.out.printf(" Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag) + " Euro \n");
		    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
		    	   eingeworfeneM�nze = tastatur.nextDouble();
		           eingezahlterGesamtbetrag += eingeworfeneM�nze;
		       }
			
		       // Fahrscheinausgabe
		       // -----------------
		       System.out.println("\nFahrschein wird ausgegeben");
		       for (int i = 0; i < 8; i++)
		       {
		          System.out.print("=");
		          try {
					Thread.sleep(250);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
		       }
		       System.out.println("\n\n");

		       // R�ckgeldberechnung und -Ausgabe
		       // -------------------------------
		       r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		       if(r�ckgabebetrag >= 0.0)
		       {
		    	   System.out.println("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + " EURO");
		    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

		           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
		           {
		        	  System.out.println("2 EURO");
			          r�ckgabebetrag -= 2.0;
		           }
		           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
		           {
		        	  System.out.println("1 EURO");
			          r�ckgabebetrag -= 1.0;
		           }
		           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
		           {
		        	  System.out.println("50 CENT");
			          r�ckgabebetrag -= 0.5;
		           }
		           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
		           {
		        	  System.out.println("20 CENT");
		 	          r�ckgabebetrag -= 0.2;
		           }
		           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
		           {
		        	  System.out.println("10 CENT");
			          r�ckgabebetrag -= 0.1;
		           }
		           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
		           {
		        	  System.out.println("5 CENT");
		 	          r�ckgabebetrag -= 0.05;
		           }
		           while(r�ckgabebetrag >= 0.02)// 2 CENT-M�nzen
		           {
		        	  System.out.println("2 CENT");
		 	          r�ckgabebetrag -= 0.02;
		           }
		           while(r�ckgabebetrag >= 0.01)// 1 CENT-M�nzen
		           {
		        	  System.out.println("1 CENT");
		 	          r�ckgabebetrag -= 0.01;
		           }
		       }

		       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
		                          "vor Fahrtantritt entwerten zu lassen!\n"+
		                          "Wir w�nschen Ihnen eine gute Fahrt.");
		    }
			
			
			
			
			
		}
		
	


